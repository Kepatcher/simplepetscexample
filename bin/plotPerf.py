#! /usr/bin/env python
import os

sizes_ilu = []
times_ilu = []
sizes_gamg = []
times_gamg = []

for k in range(5):
  Nx = 10 * 2**k
  modname = 'perfI%d' % k
  options = ['-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname, '-ksp_type gmres -pc_type ilu']
  os.system('./ex5 '+' '.join(options))
  perfmod = __import__(modname)
  sizes_ilu.append(Nx**2)
  times_ilu.append(perfmod.Stages['Main Stage']['SNESJacobianEval'][0]['time'])
print zip(sizes_ilu, times_ilu)

for k in range(5):
  Nx = 10 * 2**k
  modname = 'perfG%d' % k
  options = ['-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname, '-ksp_type gmres -pc_type gamg']
  os.system('./ex5 '+' '.join(options))
  perfmod = __import__(modname)
  sizes_gamg.append(Nx**2)
  times_gamg.append(perfmod.Stages['Main Stage']['SNESJacobianEval'][0]['time'])
print zip(sizes_gamg, times_gamg)

from pylab import legend, plot, loglog, show, title, xlabel, ylabel
plot(sizes_ilu, times_ilu, '-b', label='ILU')
plot(sizes_gamg, times_gamg, '-r', label='GAMG')
legend(loc='upper right')
title('AssemblyTime-Jacobian')
xlabel('Problem Size $N$')
ylabel('Time (s)')
show()
